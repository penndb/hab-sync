#!/usr/bin/env python

from __future__ import print_function
import boto3

import sys
import os
import uuid
import time
import os.path
from stat import *
from boto3.s3.transfer import S3Transfer

from daemon import daemon

class SyncDaemon(daemon):

    def __init__(self,pid):
        daemon.__init__(self,pid)

        if 'AWS_ACCESS_KEY_ID' in os.environ:
            self.AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
            self.AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
            self.endpoint = os.environ['AWS_ENDPOINT']
        else:
            print ("Going with default Minio/Docker config")
            self.AWS_ACCESS_KEY_ID = 'AKIAIOSFODNN7HABITAT'
            self.AWS_SECRET_ACCESS_KEY = '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'
            self.endpoint = 'http://127.0.0.1:9000'

        self.bucket_name = 'habitat1'
        self.folder_name = 'backups'

        self.done = set()
        self.visited = set()
        # home = sys.argv[1]
        self.rootdir = os.path.join(os.environ['HOME'], "Backups")
        #self.lastdir = os.path.join(os.environ['HOME'], "Backups/last")

    # Check and back up files in filepath that aren't in fileold
    def checkdir(self,basedir,filepath,fileold,transfer):
        stat = os.stat(filepath)[ST_INO]

        if not (stat in self.done):
            isLink = os.path.islink(filepath)
            if (not isLink) & (len(fileold)) > 0 & (os.path.exists(fileold)):
                stat_old = os.stat(fileold)[ST_INO]
                isLink = (stat_old == stat)
            if not isLink:
                rel_path = filepath.replace(basedir + "/", "")
                prefix = str(stat)
                dest = os.path.join(self.folder_name, rel_path, prefix)
                print("Uploading: " + dest)
                transfer.upload_file(filepath, self.bucket_name, dest, \
                                     extra_args={'Metadata': {'unique': str(stat),
                                                              'modified': str(os.path.getmtime(filepath))}})
                self.done.add(stat)

    def sorted_ls(self, path):
        mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
        return list(sorted(os.listdir(path), key=mtime))

    def run(self):
        self.client = boto3.client('s3', aws_access_key_id=self.AWS_ACCESS_KEY_ID, \
                                   aws_secret_access_key=self.AWS_SECRET_ACCESS_KEY, \
                                   endpoint_url=self.endpoint)

        transfer = S3Transfer(self.client)

        while True:
            print ('Scanning...')
            dirlist = self.sorted_ls(self.rootdir)
            dirlist.remove('last')
            dirlist.remove('current')
            for i,dir in enumerate(dirlist):
                if not dir in self.visited:
                    prefix = str(uuid.uuid4())
                    curdir = os.path.join(self.rootdir, dir)
                    if i > 0:
                        lastdir = os.path.join(self.rootdir, dirlist[i-1])
                    else:
                        lastdir = ''
                    if len(lastdir) == 0:
                        print ('Sync daemon copying ' + curdir + '...')
                    else:
                        print ('Sync daemon checking ' + curdir + ' vs ' + lastdir + "...")

                    fileold = ''
                    for root, subFolders, files in os.walk(curdir):
                        for filename in files:
                            filepath = os.path.join(root, filename)
                            if len(lastdir) > 0:
                                fileold = filepath.replace(curdir, lastdir)
                            #print (filepath + " vs " + fileold)
                            try:
                                self.checkdir(curdir, filepath, fileold, transfer)
                            except Exception as e:
                                print (e)
                    self.visited.add(dir)
            time.sleep(10)



if __name__ == "__main__":
    sd = SyncDaemon('/tmp/sync-daemon.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            sd.start()
        elif 'stop' == sys.argv[1]:
            sd.stop()
        elif 'restart' == sys.argv[1]:
            sd.restart()
        else:
            print ("Unknown command")
            sys.exit(2)
        sys.exit(0)
    else:
        print ("Usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
