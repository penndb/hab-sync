#!/bin/bash
#watchman -- trigger $HOME/Documents/Papers paperTrigger '**/*.tex' '**/*.bib' -- $HOME/habitat/hab-repo/watch/backup.sh
if [ $# -eq 0 ]
  then
      echo "Syntax: $0 {watch-root-directory}"
      exit 1
fi
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null
watchman -- trigger $1 syncTrigger '**/*' -- $SCRIPTPATH/backup.sh

