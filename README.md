# Habitat Sync Tool

This is a collection of resources for automatic backup of files, preserving versions between changes.  Every time a file in a "watched" directory tree is modified, it will be archived.  The archive is periodically replicated to Amazon S3 or the equivalent (e.g., Minio running on Docker).

## Prerequisites

* [Facebook WatchMan](https://facebook.github.io/watchman/) 
* Python (2.7+ or 3.4+)
* Linux, Linux on Windows, Cygwin, or Mac OS X; we need ``rsync`` to capture versioned backups in ``$HOME/Backup``.

## Setup

First, run ``pip install boto3``.  Next choose an option for where to archive the data.

## Archive option (A) - Habitat Minio Option

If you want to run your own Minio bucket, use the setup from the Habitat repository, which can be launched via:

```
docker run \
       -e "MINIO_ACCESS_KEY=AKIAIOSFODNN7HABITAT" \
       -e "MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYHABITAT1KY" \
       -v $POSTGRES_DATA/files:/export \
       -p 9000:9000 minio/minio server /export
```


## Archive option (B) - AWS Option

Ensure that you have an AWS S3 bucket or your own Docker instance with Minio.  

* If you are using your own S3 bucket, configure the shell environment variables ``AWS_SECRET_ACCESS_KEY``, ``AWS_ACCESS_KEY_ID`` in your environment.  Set ``AWS_ENDPOINT`` to the desired endpoint.

## Watching and Backing up a Directory Tree

1. Run ``watch.sh`` with a path to configure WatchMan to watch a directory tree.
1. Upon changes, ``backup.sh`` will be called to rsync with the Backup directory.  See ``~/Backups/current`` for the latest.
1. Run ``python sync.py start`` to start the Python daemon.
1. Run ``python sync.py stop`` to stop the Python daemon.
