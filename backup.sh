#!/bin/sh

date=`date "+%Y-%m-%dT%H:%M:%S"`
mkdir $HOME/Backups
rsync -aP --exclude '.svn' --exclude '.git' --exclude '#*' --exclude '*~' --link-dest=$HOME/Backups/current $HOME/Documents/Papers $HOME/Backups/back-$date
rm -f $HOME/Backups/last
mv $HOME/Backups/current $HOME/Backups/last
ln -s back-$date $HOME/Backups/current
